# Maintainer: Jonathon Fernyhough <jonathon_att manjaro+dot+org>

# Arch credits:
# Maintainer: Dan McGee <dan@archlinux.org>
# Contributor: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Sebastien Binet <binet@lblbox>

pkgname=('python-pip' 'python2-pip')
pkgver=19.2.3
pkgrel=1.4
pkgdesc="The PyPA recommended tool for installing Python packages"
url="https://pip.pypa.io/"
arch=('any')
license=('MIT')
_deps=('setuptools' 'appdirs' 'cachecontrol' 'colorama' 'distlib' 'distro' 'html5lib' 'lockfile'
       'msgpack' 'six' 'packaging' 'pep517' 'progress' 'pytoml' 'retrying' 'requests' 'urllib3'
       'webencodings')
makedepends=("${_deps[@]/#/python-}" "${_deps[@]/#/python2-}" 'python2-ipaddress' 'python-sphinx')
checkdepends=('python-pytest-runner' 'python-scripttest' 'python-virtualenv' 'python-pretend'
              'python-yaml' 'python-mock' 'python-freezegun' 'python-pip' 'python-wheel'
              'git' 'subversion')
backup=(etc/pip.conf)
source=("pip-${pkgver}.tar.gz::https://github.com/pypa/pip/archive/${pkgver}.tar.gz"
        pip.conf
        pip-install-upgrade.hook.in
        pip-remove.hook.in
        https://patch-diff.githubusercontent.com/raw/pypa/pip/pull/6984.patch
        https://patch-diff.githubusercontent.com/raw/pypa/pip/pull/7155.patch)
sha512sums=('59fcb79173529097662fcb9662f78fee7b00f441d49ea41f280da4b27875a425cccaa3daeda647ac7cddf936111923dc1546a3474fcdd7003777178e7143f6eb'
            '0451cace1cb58cf058356ee2fa886a40646959ce6fdc7abb49d6f4d68d80a31b90728b3bf2ebcac61d3abf55f42a7de58a4bd3547c0048f995d52f0e30ee0d20'
            '386cd1019645d616669ec3cacac197375bf93d0921398e8b90d6a5108c258d4ba7156e6a39f58a81b77d4e6f81f5d92dd09185d69ebf38c87bff36566916e7a2'
            '8156e8a1e537885615cdf90cb02f736e8432e19129cd4e254ae9a8f5dd4a2cd6de6c6fc5598697b36cefceafd8f8dec369ecbeace951001fda48c7a07e454596'
            'dfc0cc7b2bb1a3dbdfea9cadd305ffb195a6119bfe7b9201ee1f574d7cca262c0fa6f0e204fe1fc65e5687f4d0785956eab203d4197061cadf36ca2b3589600b'
            'fd8cce5ae7e542c64fa8f9e9bb5a30d61c33284d0b0f6026a389bad3269c51b6245a2153dcd8e2bc694f57b6854aa11eb621084f3d387c7bc9ef3811c4305b71')

shopt -s extglob
prepare() {
  sed "s|@PKGVER@|$pkgver|g" pip-install-upgrade.hook.in > pip-install-upgrade.hook
  sed "s|@PKGVER@|$pkgver|g" pip-remove.hook.in > pip-remove.hook

  cd "$srcdir/pip-$pkgver"

  # if virtualenv_no_global() then use_user_site = False
  patch -Np1 -i ../6984.patch
  # Implement PEP 405
  patch -Np1 -i ../7155.patch

  # Generate a full wheel for later use
  python setup.py bdist_wheel
  rm -fr build

  rm -rf src/pip/_vendor/!(__init__.py)
  sed -i -e 's/DEBUNDLED = False/DEBUNDLED = True/' \
            src/pip/_vendor/__init__.py
}

build() {
  cd "$srcdir/pip-$pkgver"

  python setup.py build
  python2 setup.py build

  cd docs/
  PYTHONPATH="$srcdir/pip-$pkgver/src/" sphinx-build -W -b man -d build/doctrees/man man build/man -c html
  mkdir -p build/man-pip2
  cd build/man
  for manfile in *; do
    sed 's/pip/pip2/g;s/PIP/PIP2/g' $manfile > ../man-pip2/${manfile/pip/pip2}
  done
}

check() {
  cd "$srcdir"/pip-$pkgver
  pip wheel -w tests/data/common_wheels -r tools/tests-common_wheels-requirements.txt
  python setup.py install --root="$PWD/tmp_install" --optimize=1
  PYTHONPATH="$PWD/tmp_install/usr/lib/python3.7/site-packages" PATH="$PWD/tmp_install/usr/bin:$PATH" \
    python setup.py pytest --addopts "-m unit --junit-xml=junit/unit-test.xml" || warning "Tests failed"
}

package_python-pip() {
  depends=("${_deps[@]/#/python-}")

  cd "$srcdir/pip-$pkgver"
  python setup.py install --prefix=/usr --root="$pkgdir"

  install -D -m644 LICENSE.txt \
	  "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  install -Dm644 -t "$pkgdir"/usr/share/man/man1 docs/build/man/*

  PYTHONPATH="$pkgdir"/usr/lib/python3.7/site-packages "$pkgdir"/usr/bin/pip completion --bash \
    | install -Dm644 /dev/stdin "$pkgdir"/usr/share/bash-completion/completions/pip

  install -Dm644 -t "$pkgdir"/etc "$srcdir"/pip.conf
  install -Dm644 -t "$pkgdir"/usr/lib/python3.7/ensurepip/_bundled dist/pip-$pkgver-py2.py3-none-any.whl
  install -Dm644 -t "$pkgdir"/usr/share/libalpm/hooks "$srcdir"/*.hook
}

package_python2-pip() {
  depends=("${_deps[@]/#/python2-}" 'python2-ipaddress')
  conflicts=('python-pyinstall')
  replaces=('python-pyinstall')

  cd "$srcdir/pip-$pkgver"
  python2 setup.py install --prefix=/usr --root="$pkgdir"

  mv "$pkgdir/usr/bin/pip" "$pkgdir/usr/bin/pip2"
  sed -i "s|#!/usr/bin/env python$|#!/usr/bin/env python2|" \
    ${pkgdir}/usr/lib/python2.7/site-packages/pip/__init__.py
  python2 -m compileall ${pkgdir}/usr/lib/python2.7/site-packages/pip/__init__.py

  install -D -m644 LICENSE.txt \
	  "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  install -Dm644 -t "$pkgdir"/usr/share/man/man1 docs/build/man-pip2/*

  PYTHONPATH="$pkgdir"/usr/lib/python2.7/site-packages "$pkgdir"/usr/bin/pip2 completion --bash \
    | install -Dm644 /dev/stdin "$pkgdir"/usr/share/bash-completion/completions/pip2
}
